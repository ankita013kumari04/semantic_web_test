import json
from django.shortcuts import render
from .models import pharmasales,drugreview
from rest_framework.views import APIView
from .serializers import *
from rest_framework.response import Response
from django.db.models import Sum,F



class data(APIView):
    def get(self,request):
        data = json.loads(request.body)
        if data['year'] and data['drug'] and data['drugclassification']:
            value1 = drugreview.objects.filter(date__year = data['year'],drugName = data['drug'])
            serializer =drugreviewserializer(value1, many = True)
            value2=pharmasales.objects.filter(year = data['year']).aggregate(Sum(data['drugclassification']))
            context = {
                "drugreview": serializer.data,
                "pharmasales1": value2,
            }
            return Response(context)
        elif data['year'] and data['drug']:
            value = drugreview.objects.filter(date__year = data['year'],drugName = data['drug'])
            serializer =drugreviewserializer(value, many = True)
            return Response(serializer.data)
        elif data['year']:
            value1 = drugreview.objects.filter(date__year = data['year'])
            serializer1 =drugreviewserializer(value1, many = True)
            value2= pharmasales.objects.filter(year = data['year']).aggregate(Sum('m01ab'),Sum('m01ae'),Sum('n02ba'),Sum('n02be'),Sum('n05b'),Sum('n05c'),Sum('r03'),Sum('r06'))
            value3= pharmasales.objects.filter(year = data['year']).annotate(total_drug=F('m01ab') + F('m01ae')+F('n02ba') + F('n02be')+F('n05b') + F('n05c')+F('r03') + F('r06')).values('id','total_drug','year')
            
            context = {
                "drugreview": serializer1.data,
                "pharmasales1": value2,
                "pharmasales2":value3
            }
            return Response(context)
        elif data['drugclassification']:
            value = pharmasales.objects.aggregate(Sum(data['drugclassification']))
            return Response(value)

            
     