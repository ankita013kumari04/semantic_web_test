from django.db import models
from django.db.models import fields
from rest_framework import serializers
from .models import *

class drugreviewserializer(serializers.ModelSerializer):
    class Meta:
        model = drugreview
        fields =['id','condition','date','drugName','rating','review','uniqueID','usefulCount']
    

class pharmasalesserializer(serializers.ModelSerializer):
    class Meta:
        model = pharmasales
        fields =['id','datum','m01ab','m01ae','n02ba','n02be','n05b','n05c','r03','r06','year']
    