from django.contrib import admin
from django.urls import path, include
from . import views
from django.views.decorators.csrf import csrf_exempt
from .views import data

urlpatterns = [
    path('',csrf_exempt(data.as_view()),name="data"),
    # path('by_year',csrf_exempt(views.year))
    
]
