from django.db import models

class pharmasales(models.Model):
    datum = models.DateField()
    m01ab = models.FloatField()
    m01ae = models.FloatField()
    n02ba = models.FloatField()
    n02be = models.FloatField()
    n05b = models.FloatField()
    n05c = models.FloatField()
    r03 = models.FloatField()
    r06 = models.FloatField()
    year = models.IntegerField()

    def __str__(self):
        return str(self.id)


class drugreview(models.Model):
    condition = models.CharField(max_length= 200)
    date = models.DateField()
    drugName = models.CharField(max_length= 200)
    rating = models.IntegerField()
    review = models.TextField()
    uniqueID = models.IntegerField()
    usefulCount = models.IntegerField()

    def __str__(self):
        return str(self.id)
